//Importar la clase express

import express from 'express';
import agregar_sitio_DF from './agregar_sitio_DF';


//Crear un objeto express
const app=express();
const puerto=3000;

// Crear una ruta

app.get("/bienvenida", (req, res)=>
{
    res.send("Bienvenido al mundo de backend Node.");
});

// ruta sumar
app.get("/sumar", (peticion, respuesta)=>
{
    let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

// ruta restar
app.get("/restar", (peticion, respuesta)=>
{
    let resultado=Number(peticion.query.a)-Number(peticion.query.b);

    respuesta.send(resultado.toString());
});


// ruta multiplicar
app.get("/multiplicar", (peticion, respuesta)=>
{
    let resultado=Number(peticion.query.a)*Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

// ruta modular
app.get("/modular", (peticion, respuesta)=>
{
    let resultado=Number(peticion.query.a)%Number(peticion.query.b);

    respuesta.send(resultado.toString());
});

// ruta dividir
app.get("/dividir", (peticion, respuesta)=>
{
    let a=Number(peticion.query.a);
    let b=Number(peticion.query.b);
    if(b===0){
        let respuesta;
        respuesta.send("No se puede dividir por cero :C")

    }else{
    let resultado=a/b;

    respuesta.send(resultado.toString());
    }
    
});

app.get("/agregar_sitio", (req, res)=>{

    res.send(agregar_sitio());
    
});

app.get("/agregar_sitio_DF", (req, res)=>{

    res.send(agregar_sitio_DF());
    
});


//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});
